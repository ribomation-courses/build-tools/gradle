def extract(map, re) {
    def result = [:]
    for (e in map) {                //Map.Entry e
        if (e.key =~ re) {          //e.getKey()
            result[e.key] = e.value //e.getValue()
        }
    }
    result
}

def javaProps = extract(System.properties, /^java.+/)
for (e in javaProps) 
    println "${e.key} = ${e.value.take(30)}"
