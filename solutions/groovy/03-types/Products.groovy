@groovy.transform.ToString
class Product {
    String name
    float price
}    
def r = new Random()
def prods = (1..5).collect {
    new Product(name:"prod-${it}", price:r.nextInt(1000)+100)
}
//println "Products: ${prods}"
//println ('-'*30)
println 'Product names: ' + prods*.name*.toUpperCase()
println ('-'*30)
println prods.sort {left,right -> right.price <=> left.price}.join('\n')