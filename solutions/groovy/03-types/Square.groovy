def sq(lst) {
    def result = []
    for (n in lst) result << n*n
    result
}

println sq( [1,2,3] )
println sq( (1..10) )

int[] arr = new int[5]
arr[0] = 3
arr[1] = 5
println sq( arr )
