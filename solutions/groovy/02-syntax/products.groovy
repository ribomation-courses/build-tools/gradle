import groovy.transform.*

@ToString(includeNames=true)
class Product {
    String  name    = 'Whatever'
    float   price   = 100
    Date    created = new Date()
}

def p = new Product(name:'Groovy book', price:100)
println "p = ${p}"
println "p.name = ${p.name}"
p.name = 'Gradle in Action'
println "p = ${p.getName()}"
p.setName('Spock Basics')
println "p = ${p.name}"
println "p = ${new Product()}"
