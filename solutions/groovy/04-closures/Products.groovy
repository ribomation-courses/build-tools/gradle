@groovy.transform.ToString
class Product {
    String name
    float price
}

def r = new Random()
def prods = (1..1000).collect {
    new Product(name:"prod-${it}", price:(100 + r.nextInt(800)))
}
println '# prods: ' + prods.size()
println ('-'*25)

println '# prods.price > 750: ' + prods.findAll {it.price > 750}.size()
println ('-'*25)

println 'avg price: ' + prods*.price.sum() / prods.size()
println ('-'*25)

println '<ul>\n' + prods.drop(150).take(5).collect {p ->
    "  <li>${p.name}: ${String.format('%.2f', p.price)} kr</li>"
}.join('\n') + '\n</ul>'
