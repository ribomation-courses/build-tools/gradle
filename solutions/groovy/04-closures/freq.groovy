#!/usr/bin/env groovy
def file     = 'shakespeare.txt' as File
int minSize  = 5
int maxWords = 10

def cli = new CliBuilder(usage:'./freq.groovy [options]')
cli.with {
    h longOpt:'help', 'Show usage information'
    f longOpt:'file', args:1, argName:'filename', 'name of input file'
    w longOpt:'max-words', args:1, argName:'maxWords', 'most frequent words'
    s longOpt:'min-size', args:1, argName:'minSize', 'minimum word size'
}
def opt = cli.parse(args)
if (opt.h) { 
    cli.usage()
    System.exit(0)
}

if (opt.f) file     = opt.f as File
if (opt.w) maxWords = opt.w as int
if (opt.s) minSize  = opt.s as int

println "file    : ${file.name}"
println "minSize : ${minSize}"
println "maxWords: ${maxWords}"
println '-------------'

def freq = [:].withDefault{0}
file.readLines().each {line ->
    line.toLowerCase()
        .split(/[^a-zA-Z]+/)
        .grep {it.size() >= minSize}
        .each {word ->
            try{ freq[word]++ } catch(x) {}
        }
}
def result = freq
    .sort {left,right -> right.value <=> left.value}
    .take(maxWords)

int W = result*.key*.size().max()
result.each {w,f ->
    println "${w.capitalize().padRight(W)}: ${f}"
}
