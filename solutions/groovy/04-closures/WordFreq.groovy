def mostFrequent(dir, max=10) {
    def freq = [:].withDefault{0}
    dir.eachFileRecurse(groovy.io.FileType.FILES) {File f ->
        if (f.name.endsWith('.groovy')) {
            f.text
             .split(/[^a-zA-Z]+/)
             .grep {it.size() > 1}
             .each {word ->
                 try{ freq[word]++ } catch(x) {}
             }
        }
    }
    freq.collect {w,f -> [w,f]}
        .sort {a,b -> b[1] <=> a[1]}
        .take(max)
        .collectEntries()
}


def dir    = /D:\CourseProjects\Gradle_for_FK_Aug_2015\solutions/ as File
def result = mostFrequent(dir, 15)
int width  = result*.key*.size().max()
result.each {word, freq -> 
    println "${word.padRight(width)}: ${freq}" 
}
''
