package ribomation.numbers.cli;

import ribomation.numbers.result.Model;

/**
 * Main entry point for the CLI version.
 *
 * @user jens
 * @date 2015-05-03
 */
public class App {
    public static void main(String[] args) {
        App app = new App();
        app.parseArgs(args);
        app.run();
    }

    private int     argument = 10;
    private boolean list     = false;
    private Model   model    = new Model();

    private void parseArgs(String[] args) {
        for (int k = 0; k < args.length; k++) {
            String arg = args[k];
            if (arg.startsWith("-n")) {
                argument = Integer.parseInt(args[++k]);
            } else if (arg.startsWith("-l")) {
                list = true;
            } else {
                System.err.printf("Unknown option: %s%n", arg);
                System.err.printf("Options: [-number <int>] [-list]%n");
            }
        }
    }

    private void run() {
        model.clear();
        
        if (list) {
            for (int k = 1; k <= argument; k++) model.compute(k);
        } else {
            model.compute(argument);
        }

        System.out.println(model);
    }

}
