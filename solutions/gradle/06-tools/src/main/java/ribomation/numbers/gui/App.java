package ribomation.numbers.gui;

import ribomation.numbers.result.Model;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Main entry point for the GUI version.
 *
 * @user jens
 * @date 2015-05-04
 */
public class App extends JFrame {
    public static void main(String[] args) {
        App app = new App();
        app.setupUI();
        app.run();
    }

    private Model         model         = new Model();
    private Configuration configuration = new Configuration();
    private ComputeAction computeAction = new ComputeAction(model, configuration);

    private void setupUI() {
        this.setTitle("Numbers - Swing GUI - Version 0.42");
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        this.setLayout(new BorderLayout());
        this.add(createInputPane(), BorderLayout.NORTH);
        this.add(createOutputPane(), BorderLayout.CENTER);
        this.add(createConfigurePane(), BorderLayout.SOUTH);
    }

    private void run() {
        this.setSize(500, 300);
        this.setLocationByPlatform(true);
        this.setVisible(true);
    }

    private JPanel createInputPane() {
        JTextField input = new JTextField();
        input.setText(String.valueOf(10));

        JLabel  lbl     = new JLabel("Argument: ");
        JButton compute = new JButton("COMPUTE");

        JPanel pane = new JPanel(new BorderLayout());
        pane.add(lbl, BorderLayout.WEST);
        pane.add(input, BorderLayout.CENTER);
        pane.add(compute, BorderLayout.EAST);

        input.addActionListener(computeAction);
        compute.addActionListener(computeAction);

        computeAction.setInput(input);
        return pane;
    }

    private JComponent createOutputPane() {
        JTextArea output = new JTextArea();
        output.setBorder(BorderFactory.createLoweredBevelBorder());
        output.setFont(new Font("Monospaced", Font.PLAIN, 14));
        output.setLineWrap(true);
        output.setWrapStyleWord(false);
        output.setEditable(false);

        JScrollPane scroll = new JScrollPane(output);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        
        computeAction.setOutput(output);
        return scroll;
    }

    private JPanel createConfigurePane() {
        JCheckBox list = new JCheckBox("List Output", configuration.list);
        list.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                configuration.list = ((JToggleButton) e.getSource()).isSelected();
            }
        });

        JRadioButton plain = createFormatButton(ResultsFormat.plain);
        JRadioButton xml   = createFormatButton(ResultsFormat.xml);
        JRadioButton json  = createFormatButton(ResultsFormat.json);
        JRadioButton csv   = createFormatButton(ResultsFormat.csv);

        ButtonGroup group = new ButtonGroup();
        group.add(plain);
        group.add(xml);
        group.add(json);
        group.add(csv);

        JPanel pane = new JPanel(new FlowLayout(FlowLayout.CENTER));
        pane.add(list);
        pane.add(plain);
        pane.add(xml);
        pane.add(json);
        pane.add(csv);

        return pane;
    }

    private JRadioButton createFormatButton(ResultsFormat fmt) {
        JRadioButton b = new JRadioButton(fmt.name().toUpperCase(), configuration.format == fmt);
        b.addActionListener(new UpdateFormat(fmt, configuration));
        return b;
    }

}
