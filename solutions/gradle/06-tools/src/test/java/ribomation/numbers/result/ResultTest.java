package ribomation.numbers.result;

import ribomation.numbers.func.CachingFunction;
import ribomation.numbers.func.Function;
import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

/**
 * Unit test for result
 *
 * @user jens
 * @date 2015-05-04
 */
public class ResultTest {
    private Result         target;
    private List<Function> functions;

    @Before
    public void setUp() throws Exception {
        target = new Result();

        functions = new ArrayList<>();
        functions.add(new CachingFunction() {
            @Override
            public String getName() {
                return "foo";
            }

            @Override
            protected BigInteger computeResult(Integer arg) {
                return BigInteger.valueOf(42);
            }
        });
    }

    @Test
    public void testAdd() throws Exception {
        assertThat(target.getValues().size(), is(0));

        target.add("foo", BigInteger.TEN, 0);
        assertThat(target.getValues().size(), is(1));
    }

    @Test
    public void testCompute() throws Exception {
        Result result = target.compute(10, functions);
        assertThat(result.getValues().size(), is(1));
        assertThat(result.getValues().get("foo").result, is(BigInteger.valueOf(42)));
    }

    @Test
    public void testToCSV() throws Exception {
        Result result = target.compute(10, functions);
        assertThat(result.toCSV("#"), containsString("10#foo#42#"));
    }
}
