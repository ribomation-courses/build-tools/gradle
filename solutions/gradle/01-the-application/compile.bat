@echo off

set SRC=src\main\java
set CLS=classes

set CLI=%SRC%\ribomation\numbers\cli
set FUN=%SRC%\ribomation\numbers\func
set RES=%SRC%\ribomation\numbers\result

mkdir %CLS%

@echo on
javac -d %CLS% %CLI%\*.java %FUN%\*.java %RES%\*.java 

@echo off
tree /f %CLS%
