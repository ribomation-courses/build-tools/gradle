#!/usr/bin/env bash
set -e
set -x

SRC=src
CLS=classes

rm -rf $CLS
mkdir -p $CLS
javac -d $CLS `find $SRC -type f -name '*.java'`
tree $CLS
