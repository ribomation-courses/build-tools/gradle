#!/usr/bin/env bash
set -e
set -x

CLS=classes
APP=ribomation.numbers.cli.App

java -cp $CLS $APP $*
