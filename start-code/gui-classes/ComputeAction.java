package ribomation.numbers.gui;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.simpleframework.xml.core.Persister;
import ribomation.numbers.result.Model;
import ribomation.numbers.result.Result;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Performs the computation
 *
 * @user jens
 * @date 2015-05-04
 */
public class ComputeAction implements ActionListener {
    private Model         model;
    private Configuration configuration;
    private JTextField    input;
    private JTextArea     output;

    public ComputeAction(Model model, Configuration configuration) {
        this.model = model;
        this.configuration = configuration;
    }

    public void setInput(JTextField input) {
        this.input = input;
    }

    public void setOutput(JTextArea output) {
        this.output = output;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        output.setText("");

        String text = input.getText();
        if (text==null || text.trim().length()==0) return;

        model.clear();
        int argument = Integer.parseInt(text);
        if (configuration.list) {
            for (int k = 1; k <= argument; k++) model.compute(k);
        } else {
            model.compute(argument);
        }

        output.setText(render(model, configuration));
    }

    public String render(Model model, Configuration configuration) {
        List<String> resultsList = new ArrayList<>();
        for (Result result : model.getResults()) {
            resultsList.add(render(result, configuration));
        }
        return resultsList.toString();
    }

    public String render(Result result, Configuration configuration) {
        String payload = "";
        switch (configuration.format) {
            case plain:
                payload = result.toString();
                break;
            case xml:
                StringWriter buf       = new StringWriter();
                Persister persister = new Persister();
                try {
                    persister.write(result, buf);
                    payload = buf.toString();
                } catch (Exception ignore) {}
                break;
            case json:
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                payload   = gson.toJson(result);
                break;
            case csv:
                payload = result.toCSV();
                break;
        }

        return payload;
    }
}
